//Image blocks collection
var Blocks = Backbone.Collection.extend({
  url: 'http://5.196.17.218/imgs/images.php'
})

const config = {
  indexRef: 0,
  //you may config the below as you want, for example to display 5 blocks, just change the current number to 5
  imgsGroup: 4
}

var ImgContainerView = Backbone.View.extend({

  el: $('.carousel'),

  initialize: function() {
    this.template = _.template($('.block-template').html())
    this.collection.fetch();
    this.listenTo(this.collection, 'sync', this.render.bind(this))
  },

  events: {
    'click .btn-next': 'nextBlocks',
    'click .btn-prev': 'prevBlocks'
  },

  nextBlocks: function() {
    config.indexRef += config.imgsGroup
    this.render(config.indexRef)
  },

  prevBlocks: function() {
    config.indexRef -= config.imgsGroup
    this.render(config.indexRef)
  },

  checkButtons: function() {
    if (config.indexRef === 0) {
      this.$('.btn-prev').prop('disabled', true)
    } else if ((config.indexRef + config.imgsGroup) >= this.collection.length) {
      this.$('.btn-next').prop('disabled', true)
    }
  },

  render: function(carouselIndex) {
    if(typeof carouselIndex === 'object' || ! carouselIndex) carouselIndex=0;

    this.$el.html(this.template({
      blocks: this.collection.toJSON().slice(0 + carouselIndex, config.imgsGroup + carouselIndex).map(function(block){
        block.image = block.images[_.random(block.images.length === 0 ? 0 : block.images.length - 1)];
        return block;
      })
    }))
    this.checkButtons()
  }
})

var imgContainerView = new ImgContainerView({ collection: new Blocks() })

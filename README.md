## A carousel built using BackboneJS


### General information

This carousel is built using BackboneJS and it has minimalistic features :

 - Displays 4 blocks of images.
 - View the next/previous 4 blocks by clicking next/previous.
 - Previous button is disabled (if user is at the very beginning of carousel or at the end of it).
 - Displays a random image for each block from the set of images.
 
 - The source of carousel block is endpoint on server that return following JSON format:

     [{
     title: "First Block",
     images: [url1, url2, url3]
     },
     {
     title: "Second Block",
     images: [url7, url8]
     }
     ,...]

### Config

By default the carousel shows 4 blocks of images; however you may want to change the number of blocks by changing it from here:

      //For example to show 5 blocks, set it to 5
      imgsGroup: 4

However, for the purpose of demonstration, the JSON is hosted on a third party server :

    http://5.196.17.218/imgs/images.php

For your convenience, you may access to a live version through the following link:

    http://5.196.17.218/exe/pub/index.html

### Note

The project was built using the build tool **Brunch.io**

module.exports = {

  npm: {
    enabled: true,
    globals: {
      $: "jquery/dist/jquery.js",
      _:"underscore/underscore.js",
      Backbone: "backbone/backbone-min.js"
    },
    styles: {
      bootstrap: ['dist/css/bootstrap.css']
    }
  },
  files: {
    javascripts: {
      joinTo: 'app.js'
    },
    stylesheets: {
      joinTo: 'app.css'
    }
  }

}
